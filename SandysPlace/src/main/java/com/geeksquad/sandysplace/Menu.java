package com.geeksquad.sandysplace;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by jasonedstrom on 7/30/13.
 */
public class Menu {

    private ArrayList<Day> mDays;

    private static Menu sMenu;
    private Context mAppContext;


    private Menu(Context appContext) {
        mAppContext = appContext;
        mDays = new ArrayList<Day>();

    }

    public static Menu get(Context m) {

        if (sMenu == null){
            sMenu = new Menu(m.getApplicationContext());
        }

        return sMenu;
    }

    public ArrayList<Day> getDays() {

        return mDays;
    }


    //These are temporary for the sake of testing.
    public Menu (){
        mDays = new ArrayList<Day>();
    }

    public void addDay(Day day){
        mDays.add(day);
    }
}

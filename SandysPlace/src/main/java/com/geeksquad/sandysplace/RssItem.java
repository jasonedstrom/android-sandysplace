package com.geeksquad.sandysplace;

/**
 * Created by jasonedstrom on 7/31/13.
 */
public class RssItem {

    private String mTitle;
    private String mDescription;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }
}

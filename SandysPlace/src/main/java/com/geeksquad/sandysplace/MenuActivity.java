package com.geeksquad.sandysplace;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;


public class MenuActivity extends Activity {

    private RssFeed feed;
    private Button mDayOneButton;
    private Button mDayTwoButton;
    private Button mDayThreeButton;
    private Button mDayFourButton;
    private Button mDayFiveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        /*try{
            feed = new RssFeed();
           new RetreiveFeedTask().execute("http://www.cafebonappetit.com/rss/menu/472.xml");


        }catch (Exception e){
            e.printStackTrace();
        }*/

        mDayOneButton = (Button) findViewById(R.id.day_one);
        mDayOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mDayTwoButton = (Button) findViewById(R.id.day_two);
        mDayTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mDayThreeButton = (Button) findViewById(R.id.day_three);
        mDayThreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mDayFourButton = (Button) findViewById(R.id.day_four);
        mDayFourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mDayFiveButton = (Button) findViewById(R.id.day_five);
        mDayFiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }



}

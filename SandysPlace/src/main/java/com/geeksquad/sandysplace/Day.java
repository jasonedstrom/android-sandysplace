package com.geeksquad.sandysplace;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by jasonedstrom on 7/30/13.
 */
public class Day {

    private ArrayList<Section> mSections;
    private Date mDate;
    private String mTitleDate;


    private UUID mId;

    public Day(){
        mId = UUID.randomUUID();
        mDate = new Date();
        mSections =  new ArrayList<Section>();

    }


    public UUID getId() {
        return mId;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public ArrayList<Section> getSections() {
        return mSections;
    }

    public void addSection(Section section){
        mSections.add(section);
    }

    public String getTitleDate() {
        return mTitleDate;
    }

    public void setTitleDate(String titleDate) {
        mTitleDate = titleDate;
    }
}

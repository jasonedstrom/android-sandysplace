package com.geeksquad.sandysplace;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by jasonedstrom on 7/31/13.
 */
public class RssFeed {

    private String mTitle;
    private String mDescription;
    private ArrayList<RssItem> mRssItems;
    private int mItemCount;

    public RssFeed(){

        mRssItems = new ArrayList<RssItem>();
        mItemCount = 0;
    }

    public void addRssItem(RssItem item){
        mRssItems.add(item);
        mItemCount++;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public ArrayList<RssItem> getRssItems() {
        return mRssItems;
    }

    public int getItemCount() {
        return mItemCount;
    }

    public RssFeed getFeed(String urlToRssFeed)
    {
        try
        {
            // setup the url
            URL url = new URL(urlToRssFeed);

            // create the factory
            SAXParserFactory factory = SAXParserFactory.newInstance();
            // create a parser
            SAXParser parser = factory.newSAXParser();

            // create the reader (scanner)
            XMLReader xmlreader = parser.getXMLReader();
            // instantiate our handler
            RssHandler theRssHandler = new RssHandler();
            // assign our handler
            xmlreader.setContentHandler(theRssHandler);
            // get our data through the url class
            InputSource is = new InputSource(url.openStream());
            // perform the synchronous parse
            xmlreader.parse(is);
            // get the results - should be a fully populated RSSFeed instance,
            // or null on error
            return theRssHandler.getFeed();
        }
        catch (Exception ee)
        {
            ee.printStackTrace();
            // if you have a problem, simply return null
            return null;
        }
    }
}

package com.geeksquad.sandysplace;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by jasonedstrom on 7/30/13.
 */
public class Section {

    private ArrayList<String> mDishes;
    private ArrayList<String> mDescriptions;
    private String mDescription;
    private String mTitle;
    private String mDish;
    private Date mDate;
    private UUID mId;
    private boolean mMulitpleItems;
    private boolean mFavorite;



    public Section(String title, ArrayList<String> dishes, ArrayList<String> descriptions, Date date){
        mTitle = title;
        //mDish = dish;
        //mDescription = description;
        mDate = date;
        mId = UUID.randomUUID();
        mDishes = dishes;
        mDescriptions = descriptions;
        mMulitpleItems = true;
    }

    public Section(String title, String dish, String description, Date date){
        mTitle = title;
        mDish = dish;
        mDescription = description;
        mDate = date;
        mId = UUID.randomUUID();
        mMulitpleItems = false;
    }

    public UUID getId() {
        return mId;
    }

    public String getDish() {

        return mDish;
    }

    public void setDish(String dish) {
        mDish = dish;
    }

    public String getTitle() {

        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {

        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public boolean isFavorite() {

        return mFavorite;
    }

    public void setFavorite(boolean favorite) {
        mFavorite = favorite;
    }

    public Date getDate() {

        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public boolean isMulitpleItems() {
        return mMulitpleItems;


    }
}

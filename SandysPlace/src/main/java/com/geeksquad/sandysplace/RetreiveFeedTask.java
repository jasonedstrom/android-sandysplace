package com.geeksquad.sandysplace;

import android.os.AsyncTask;
import android.text.Html;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by jasonedstrom on 7/31/13.
 */
public class RetreiveFeedTask extends AsyncTask<String, Void, RssFeed> {
    private Menu mMenu;
    private Exception exception;

    protected RssFeed doInBackground(String... urls) {
        try {
            URL url= new URL(urls[0]);
            SAXParserFactory factory =SAXParserFactory.newInstance();
            SAXParser parser=factory.newSAXParser();
            XMLReader xmlreader=parser.getXMLReader();
            RssHandler theRSSHandler=new RssHandler();
            xmlreader.setContentHandler(theRSSHandler);
            InputSource is=new InputSource(url.openStream());
            xmlreader.parse(is);
            return theRSSHandler.getFeed();
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }

    protected void onPostExecute(RssFeed feed) {
        mMenu = new Menu();
        RssFeed mFeed = feed;
        ArrayList<RssItem> mItems = feed.getRssItems();
        //RssItem item = mItems.get(0);
        for (RssItem item: mItems){
        Day mDay = new Day();
        mDay.setTitleDate(item.getTitle());
        Date convertedDate = new Date();
        try {
            convertedDate = new SimpleDateFormat("EEE, d MMMM yyyy", Locale.ENGLISH).parse(item.getTitle());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mDay.setDate(convertedDate);
        String htmlStr = item.getDescription();
        //String TextFromHtml = Html.fromHtml(htmlStr).toString();



        /*while(htmlStr.length() > 0){

        int indexOfOpen = htmlStr.indexOf("<h4>") + 4;
        int indexOfClosed = htmlStr.indexOf("</h4>");
        String tempString = htmlStr.substring(indexOfOpen, indexOfClosed);
        String title = tempString.substring(tempString.indexOf("[")+1, tempString.indexOf("]"));
        String dish = tempString.substring(tempString.indexOf("]")+2, tempString.length());
        tempString = htmlStr.substring(0, indexOfClosed+5);
        htmlStr = htmlStr.replace(tempString, "");
        indexOfOpen = htmlStr.indexOf("<p>") + 3;
        indexOfClosed = htmlStr.indexOf("</p>");
        tempString = htmlStr.substring(indexOfOpen, indexOfClosed);
        String description = tempString;
        Section mSection = new Section(title, dish, description, convertedDate);
        mDay.addSection(mSection);
        tempString = htmlStr.substring(0, indexOfClosed+4);
        htmlStr = htmlStr.replace(tempString, "");

        }*/

        Pattern itemDescriptionPattern = Pattern.compile("\\[.+?\\].+?</p>");
        Pattern sectionsPattern = Pattern.compile("\\[.+?\\]");
        Pattern dishPattern = Pattern.compile("\\].+?<");
        Pattern descriptionPattern = Pattern.compile("<p>(.*?)</p>");
        Matcher matcher = itemDescriptionPattern.matcher(htmlStr);
        ArrayList<String> matches = new ArrayList<String>();

        while (!(matcher.hitEnd())){

            if (matcher.find()){
                //String match = matcher.group();
                matches.add(matcher.group());

            }
        }

        ArrayList<String> sections = new ArrayList<String>();


        for (String m : matches){
            matcher = sectionsPattern.matcher(m);

             if (matcher.find()){
                 String tempSection = matcher.group();
                //tempSection = tempSection.replace("[","");
                //tempSection = tempSection.replace("]","");
                 if (!(sections.contains(tempSection))){
                     sections.add(tempSection);
                 }
             }

            }

        for (String s: sections){
            int count =0;
            ArrayList<String> dishes = new ArrayList<String>();
            for (String m: matches){

                if (m.contains(s)){
                    count++;
                    dishes.add(m);
                }
            }

                if (dishes.size() == 1){
                   for (String d : dishes){

                      matcher = dishPattern.matcher(d);
                       String tempDish = new String();
                       String tempDescription = new String();

                       if (matcher.find()){
                       tempDish = matcher.group();
                       tempDish = tempDish.replace("] ","");
                       tempDish = tempDish.replace("<","");
                       }


                       matcher = descriptionPattern.matcher(d);

                       if (matcher.find()){
                       tempDescription = matcher.group();
                       tempDescription = tempDescription.replace("<p>","");
                       tempDescription = tempDescription.replace("</p>","");
                       }

                       Section mSection = new Section(s, tempDish, tempDescription, convertedDate);
                       mDay.addSection(mSection);

                   }

                } else if (dishes.size() > 1){
                    ArrayList<String> addDishes = new ArrayList<String>();
                    ArrayList<String> addDescriptions = new ArrayList<String>();
                    for (String d : dishes){

                        matcher = dishPattern.matcher(d);
                        String tempDish = new String();
                        String tempDescription = new String();

                        if (matcher.find()){
                            tempDish = matcher.group();
                            tempDish = tempDish.replace("] ","");
                            tempDish = tempDish.replace("<","");
                        }

                        addDishes.add(tempDish);

                        matcher = descriptionPattern.matcher(d);

                        if (matcher.find()){
                            tempDescription = matcher.group();
                            tempDescription = tempDescription.replace("<p>","");
                            tempDescription = tempDescription.replace("</p>","");
                        }

                        addDescriptions.add(tempDescription);

                    }

                    Section mSection = new Section (s, addDishes, addDescriptions, convertedDate);
                    mDay.addSection(mSection);
                }

        }

        //System.out.println("Done");
        mMenu.addDay(mDay);
        }

    }

    }

